# Machine Provisioning

License: [CC0-1.0](LICENSE)

Repo: https://gitlab.com/liamdawson/provision-machine

Liam Dawson's personal machine provisioning scripts.

## Instructions

### Ubuntu Dell XPS 15

#### After new install

```shell
# enable network, then...

sudo apt-get install -y curl
mkdir -p ~/workspace/provision-machine
cd ~/workspace/provision-machine
curl -sSL https://gitlab.com/liamdawson/provision-machine/-/archive/master/provision-machine-master.tar.bz2 | tar xj --strip-components=1
# preview states to apply, generate bytecode as user
python3 provision.py ubuntu xps15
# apply states
sudo !! --apply
```
